﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1.models
{
  public  class TaskModel
    {
    private String id;//Java UUID
    private String padId;
    private DateTime date;
    private int userId = -1;
    private String title;
    private bool completed;
    private int sortOrder;
    private bool deleted;
        private bool isStar;
        private bool sync;

        public TaskModel(string id, string padId, DateTime date, int userId, string title, bool completed, int sortOrder, bool deleted, bool isStar, bool sync)
        {
            this.Id = id;
            this.PadId = padId;
            this.Date = date;
            this.UserId = userId;
            this.Title = title;
            this.Completed = completed;
            this.SortOrder = sortOrder;
            this.Deleted = deleted;
            this.IsStar = isStar;
            this.Sync = sync;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string PadId
        {
            get
            {
                return padId;
            }

            set
            {
                padId = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }

        public bool Completed
        {
            get
            {
                return completed;
            }

            set
            {
                completed = value;
            }
        }

        public int SortOrder
        {
            get
            {
                return sortOrder;
            }

            set
            {
                sortOrder = value;
            }
        }

        public bool Deleted
        {
            get
            {
                return deleted;
            }

            set
            {
                deleted = value;
            }
        }

        public bool IsStar
        {
            get
            {
                return isStar;
            }

            set
            {
                isStar = value;
            }
        }

        public bool Sync
        {
            get
            {
                return sync;
            }

            set
            {
                sync = value;
            }
        }


    }
}
