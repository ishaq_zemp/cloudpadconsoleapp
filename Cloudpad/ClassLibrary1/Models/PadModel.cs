﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1.models
{
 public  class PadModel
    {
         private String id;
         private String padTypeCode;
         private int userId = -1;//initially won't use it
         private String title;
         private int sortOrder;
         private bool deleted;//boolean
         private  bool sync;
         private bool shared;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string PadTypeCode
        {
            get
            {
                return padTypeCode;
            }

            set
            {
                padTypeCode = value;
            }
        }

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }

        public int SortOrder
        {
            get
            {
                return sortOrder;
            }

            set
            {
                sortOrder = value;
            }
        }

        public bool Deleted
        {
            get
            {
                return deleted;
            }

            set
            {
                deleted = value;
            }
        }

        public bool Sync
        {
            get
            {
                return sync;
            }

            set
            {
                sync = value;
            }
        }

        public bool Shared
        {
            get
            {
                return shared;
            }

            set
            {
                shared = value;
            }
        }

        public PadModel(string id, string padTypeCode, int userId, string title, int sortOrder, bool deleted, bool sync, bool shared)
        {
            this.Id = id;
            this.PadTypeCode = padTypeCode;
            this.UserId = userId;
            this.Title = title;
            this.SortOrder = sortOrder;
            this.Deleted = deleted;
            this.Sync = sync;
            this.Shared = shared;
        }
    }
}
