﻿using ClassLibrary1.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1.Helpers
{
    class DummyDataHelper
    {
        public static List<TaskModel> getTasksDummyData()
        {
            return new List<TaskModel>() {
              new TaskModel("t1", "1",new DateTime(),321,"HomeWork",false,1,false,false, false),
              new TaskModel("t2", "1",new DateTime(),321,"write app for iOS",false,2,false,false, false),
              new TaskModel("t3", "1",new DateTime(),321,"upgrading startpot level 7",false,3,false,false, false),
              new TaskModel("t4", "2",new DateTime(),321,"after startport upgrade to Engery Storage",false,10,false,false, false),
              new TaskModel("t5", "1",new DateTime(),321,"After Targeting by target",false,4,false,false, false),
              new TaskModel("t6", "1",new DateTime(),321,"Moring in Serwakai",false,5,false,false, false),
              new TaskModel("t7", "1",new DateTime(),321,"Fast Blowing Air Mola khan",false,6,false,false, false),
              new TaskModel("t8", "2",new DateTime(),321,"Dummy Data for iOS app",false,7,false,false, false),
              new TaskModel("t9", "2",new DateTime(),321,"In sha Allah it will be the best App",false,8,false,false, false),
              new TaskModel("t0", "2",new DateTime(),321,"Allah hume is ko pure karne me kamyabe de ameen",false,9,false,false, false),
              };
        }

        public static List<PadModel> getPadsDummyData()
        {
            return new List<PadModel>()
        {
            new PadModel("1","1",321,"ishaq Works",1,false,false,false),
            new PadModel("2","1",321,"Dummy Pad",2,false,false,false)
        };
        }
    }
}
