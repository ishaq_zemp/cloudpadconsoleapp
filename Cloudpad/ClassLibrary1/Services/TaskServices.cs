﻿using ClassLibrary1.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1.Services
{
    interface TaskServices : DBServices
    {
        TaskModel getTaskByTaskId(String taskId);
        List<TaskModel> getTasksByPadId(String padId);
    }
}
