﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1.Services
{
    interface DBServices
    {
        void save(Object model);
        void save(List<Object> models);
        void update(Object model);
        void delete(Object model);
  
    }
}
