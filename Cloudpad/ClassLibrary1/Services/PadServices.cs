﻿using ClassLibrary1.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1.Services
{
    interface PadServices : DBServices
    {
        PadModel getPadByPadId(String padId);
        List<PadModel> getPadsByUserId(int userId);
    }
}
