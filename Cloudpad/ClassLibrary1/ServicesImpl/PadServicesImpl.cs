﻿using ClassLibrary1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary1.models;
using ClassLibrary1.Helpers;

namespace ClassLibrary1.ServicesImpl
{
    public class PadServicesImpl : PadServices
    {
        List<PadModel> listOfPads = DummyDataHelper.getPadsDummyData();
        public void delete(object model)
        {
            throw new NotImplementedException();
        }

        public PadModel getPadByPadId(string padId)
        {
           
            foreach (PadModel padModel in listOfPads)
            {
                if (padId.Equals(padModel.Id))
                    return padModel;
            }
            return null;
        }

        public PadModel getPadByTaskId(string taskId)
        {
            return null;
        }

        public List<PadModel> getPadsByUserId(int userId)
        {
            List<PadModel> pads = new List<PadModel>();
            foreach (PadModel padModel in listOfPads)
            {
                if (userId.Equals(padModel.UserId))
                    pads.Add(padModel);
            }
            return pads;
        }

        public void save(List<object> models)
        {
            throw new NotImplementedException();
        }

        public void save(object model)
        {
            throw new NotImplementedException();
        }

        public void update(object model)
        {
            throw new NotImplementedException();
        }
    }
}
