﻿using ClassLibrary1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary1.models;
using ClassLibrary1.Helpers;

namespace ClassLibrary1.ServicesImpl
{
public    class TaskServicesImpl : TaskServices
    {
        List<TaskModel> listofTasks = DummyDataHelper.getTasksDummyData();
        public void delete(object model)
        {
            throw new NotImplementedException();
        }

        public TaskModel getTaskByTaskId(string taskId)
        {
           

            foreach(TaskModel taskModel in listofTasks)
            {
                if (taskId.Equals(taskModel.Id))
                {
                    return taskModel;
                }
            }
            return null;
        }

        public List<TaskModel> getTasksByPadId(string padId)
        {
            List<TaskModel> tasks = new List<TaskModel>();
            foreach (TaskModel taskModel in listofTasks)
            {
                if (padId.Equals(taskModel.PadId))
                {
                    tasks.Add(taskModel);
                }
               
            }
            return tasks;
        }

        public void save(List<object> models)
        {
            throw new NotImplementedException();
        }

        public void save(object model)
        {
            throw new NotImplementedException();
        }

        public void update(object model)
        {
            throw new NotImplementedException();
        }
    }
}
