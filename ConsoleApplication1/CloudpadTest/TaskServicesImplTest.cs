﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary1.ServicesImpl;
using System.Collections.Generic;
using ClassLibrary1.models;

namespace CloudpadTest
{
    [TestClass]
    public class TaskServicesImplTest
    {
        private TaskServicesImpl taskServices;

        [TestInitialize]
        public void initTest()
        {
            taskServices = new TaskServicesImpl();
        }
        [TestMethod]
        public void getTaskByTaskIdTest()
        {
            //Arrange
            string expectedTaskId = "t1";
            //Act
            string actualTaskId = taskServices.getTaskByTaskId(expectedTaskId).Id;
            //Assert
            Assert.AreEqual(expectedTaskId, actualTaskId);
        }
        [TestMethod]
        public void getTasksByPadIdTest()
        {
            //Arrange
            string expectedPadId = "1";
            List<TaskModel> taskModels;

            //Act

            taskModels = taskServices.getTasksByPadId(expectedPadId);

            foreach(TaskModel taskModel in taskModels)
            {
                //Assert
                Assert.AreEqual(expectedPadId, taskModel.PadId);
                
            }

            

        }
    }
}
