﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary1.ServicesImpl;

namespace CloudpadTest
{
    [TestClass]
    public class PadServices
    {
        PadServicesImpl padServices;
        [TestInitialize]
        public void testInit()
        {
            padServices =  new PadServicesImpl();
        }
        [TestMethod]
        public void getPadByPadIdTest()
        {
            //Arrange
            string expectedPadId = "1";
            //Act
            string actualPadId = padServices.getPadByPadId("1").Id;
            //Assertion
            Assert.AreEqual(expectedPadId, actualPadId);
        }
        [TestMethod]
        public void getPadByUserIdTest()
        {
            //Arrange
            int userId = 321;
            //Action
            int actualUserId = padServices.getPadsByUserId(userId)[0].UserId;
            //Assert
            Assert.AreEqual(userId, actualUserId);


        }
        
    
    }
}
