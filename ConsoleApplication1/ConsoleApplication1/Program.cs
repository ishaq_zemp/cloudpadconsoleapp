﻿using ClassLibrary1;
using ClassLibrary1.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1.ServicesImpl;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskModel taskModel = new TaskServicesImpl().getTaskByTaskId("t1");
            showTextOnConsole("Task Model");

            showTaskOnConsole(taskModel);
            PadModel padModel = new PadServicesImpl().getPadByPadId("2");
            List<TaskModel> listOfModels = new TaskServicesImpl().getTasksByPadId(padModel.Id);
            showTextOnConsole("#=========List of Task Models=========#");
            showListOfTaskOnConsole(listOfModels);
            List<PadModel> listOfPadsByUserId = new PadServicesImpl().getPadsByUserId(321);
            showTextOnConsole("#=========List of Pad Models==========#");
            showListOfPadsOnConsole(listOfPadsByUserId);
           int one =  Console.Read();
            showTextOnConsole("input is: " + one);
            Console.Read();
           
        }
        
        public static void showTaskOnConsole(TaskModel taskModel)
        {
            showTextOnConsole("---" + taskModel.Title + "---");
            Console.WriteLine("Id: " + taskModel.Id);
            Console.WriteLine("Title: " + taskModel.Title);
            Console.WriteLine("Pad Id: " + taskModel.PadId);
            Console.WriteLine("SortOrder: " + taskModel.SortOrder);
            Console.WriteLine("Sync: " + taskModel.Sync);
            Console.WriteLine("Star: " + taskModel.IsStar);
            Console.WriteLine("Completed: " + taskModel.Completed);
            
           
        }
        public static void showTextOnConsole(String text)
        {
            Console.WriteLine(text);
        }
        
        public static void showPadOnConsole(PadModel padModel)
        {
            Console.WriteLine("====" + padModel.Title + "===");
            Console.WriteLine("Id: " + padModel.Id);
            Console.WriteLine("Title: " + padModel.Title);
            Console.WriteLine("Pad Type Code: " + padModel.PadTypeCode);
            Console.WriteLine("User Id:" + padModel.UserId);
            Console.WriteLine("Sort Order: " + padModel.SortOrder);
            Console.WriteLine("Deleted: " + padModel.Deleted);
         

        }
        public static void showListOfPadsOnConsole(List<PadModel> pads)
        {
            foreach(PadModel pad in pads)
            {
                showPadOnConsole(pad);
            }
        }

        public static void showListOfTaskOnConsole(List<TaskModel> tasks)
        {
            foreach (TaskModel model in tasks)
            {
                showTaskOnConsole(model);
            }
        }
    }

  
}
